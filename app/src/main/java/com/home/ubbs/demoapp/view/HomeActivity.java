package com.home.ubbs.demoapp.view;

import android.os.Bundle;

import com.home.ubbs.demoapp.view.fragments.HomeFragment;

/**
 * Created by udyatbhanu-mac on 11/4/17.
 */

public class HomeActivity extends NavigationDrawerActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeView(HomeFragment.newInstance());

    }


}
