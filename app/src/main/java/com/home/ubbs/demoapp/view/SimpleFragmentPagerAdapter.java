package com.home.ubbs.demoapp.view;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.home.ubbs.core.PlaceHolderFragment;

/**
 * Created by udyatbhanu-mac on 11/5/17.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new PlaceHolderFragment();
        } else if (position == 1){
            return new PlaceHolderFragment();
        } else if (position == 2){
            return new PlaceHolderFragment();
        } else {
            return new PlaceHolderFragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 4;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Test1";
            case 1:
                return "Test1";
            case 2:
                return "Test1";
            case 3:
                return "Test1";
            default:
                return null;
        }
    }
}
