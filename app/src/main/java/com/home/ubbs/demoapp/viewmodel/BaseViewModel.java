package com.home.ubbs.demoapp.viewmodel;

import android.databinding.BaseObservable;

/**
 * Created by udyatbhanu-mac on 10/28/17.
 */

public abstract class BaseViewModel extends BaseObservable {
     protected abstract void reset();
}
