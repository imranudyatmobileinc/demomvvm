package com.home.ubbs.demoapp.view;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.home.ubbs.core.BaseFragment;
import com.home.ubbs.core.PlaceHolderFragment;
import com.home.ubbs.core.menu.NavigationMenuAdapter;
import com.home.ubbs.demoapp.R;
import com.home.ubbs.demoapp.databinding.NavigationDrawerActivityBinding;
import com.home.ubbs.demoapp.viewmodel.NavigationDrawerActivityViewModel;

public abstract class NavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    NavigationDrawerActivityBinding navigationDrawerActivityBinding;
    NavigationDrawerActivityViewModel navigationDrawerActivityViewModel;
    FragmentManager fragmentManager;


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);





        navigationDrawerActivityBinding = DataBindingUtil.setContentView(this, R.layout.navigation_drawer_activity);
        navigationDrawerActivityBinding.setNavigationDrawerViewModel(navigationDrawerActivityViewModel);
        setSupportActionBar(navigationDrawerActivityBinding.navigationDrawer.toolbar);
        navigationDrawerActivityViewModel = new NavigationDrawerActivityViewModel(this,
                navigationDrawerActivityBinding.drawerLayout);
        DrawerLayout drawer = navigationDrawerActivityBinding.drawerLayout;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, navigationDrawerActivityBinding.navigationDrawer.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();



        NavigationView navigationView = navigationDrawerActivityBinding.navView;
        navigationView.setNavigationItemSelectedListener(this);


        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

        String []myDataset = new String[4];

        myDataset[0] = "Item1";
        myDataset[1] = "Item2";
        myDataset[2] = "Item3";
        myDataset[3] = "Item4";


        mAdapter = new NavigationMenuAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);



        if (savedInstanceState == null) {
            Fragment fragment = new PlaceHolderFragment();
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent i =  new Intent(this, MainActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     *
     * @param fragment
     */
    protected void initializeView(BaseFragment fragment){

        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

    }
}
