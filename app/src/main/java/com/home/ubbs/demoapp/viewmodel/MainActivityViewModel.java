package com.home.ubbs.demoapp.viewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.home.ubbs.demoapp.R;

/**
 * Created by udyatbhanu-mac on 10/28/17.
 */

public class MainActivityViewModel extends BaseViewModel{

    private Context context;

    public ObservableField<String> messageLabel;

    public MainActivityViewModel(Context context){
        this.context = context;
        messageLabel = new ObservableField<>(context.getString(R.string.message_label));
    }


    /**
     * The view model takes care of the action, the activity does not handel this in MVVM
     * @param view
     */
    public void onClickFabLoad(View view) {
        Snackbar.make(view, "Replace with your own actions", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }


    @Override
    protected void reset() {
        context = null;  // always set the context to null or it might cause a memory leak
    }
}
